/*
    ENDPOINT                       PARAMS               METHOD
*/
const result = require('dotenv').config();


/* This check doesn't work with Heroku :( */

// if (result.error) {
//     console.log("Unable to load \".env\" file. Please provide one to store the JWT secret key");
//     process.exit(-1);
// }
// if (!process.env.JWT_SECRET) {
//     console.log("\".env\" file loaded but JWT_SECRET=<secret> key-value pair was not found");
//     process.exit(-1);
// }


import express = require('express');
import mongoose = require('mongoose');
import jsonwebtoken = require('jsonwebtoken');  // JWT generation
import jwt = require('express-jwt');            // JWT parsing middleware for express
import http = require('http');                // HTTP module
import https = require('https');
import io = require('socket.io');
import passport = require('passport');           // authentication middleware for express
import passportHTTP = require('passport-http');
import bodyparser = require('body-parser');
import cors = require('cors');
const { ObjectId } = require('mongodb');

//Local import
import * as user from './User'
import { User } from './User';
import * as book from './Book'
import * as inserzioni from './Inserzioni'
import * as message from './Message'
import { Book } from "./Book";


var ios = undefined;

var app = express();
var auth = jwt({ secret: process.env.JWT_SECRET });

app.use(bodyparser.json());
app.use(cors());

const port = process.env.PORT || 8080;

app.get("/", (req, res) => {
    res.status(200).json({ api_version: "1.0", endpoints: ["/user", "/book"] });
    // json method sends a JSON response (setting the correct Content-Type) to the client
});

/**
 * 
 * INSERZIONI
 * 
 */
//Ritorna tutte le inserzioni
app.get("/inserzioni", function (req, res) {
    inserzioni.getModel().find().then(function (documents) {
        if (documents.length == 0) {
            return res.status(400).json({ statusCode: 404, error: true, message: 'Nessuna inserzione trovata' });
        }
        else {
            return res.status(200).json(documents);
        }
    }).catch(function (reason) {
        return res.status(400).json({ statusCode: 404, error: true });
    });
})

//Ritorna le inserzioni attive
app.get('/inserzioni/active', (req, res, next) => {
    inserzioni.getModel().find({ active: true }).then(function (documents) {
        return res.status(200).json(documents);
        // }
    }).catch((reason) => {
        return next({ statusCode: 404, error: true, errormessage: "DB error: " + reason });
    });
})

//Ritorna le inserzioni di un utente
app.get('/inserzioni/user/:username', (req, res, next) => {
    inserzioni.getModel().find({ insertByUser: req.params.username }).then((u) => {
        return res.status(200).json(u);
    }).catch((reason) => {
        return next({ statusCode: 404, error: true, errormessage: "DB error: " + reason });
    });
})

//Inserisce l'inserzione
app.post("/inserzioni", auth, async function (req, res, next) {
    user.getModel().findOne({ username: req.user.username }).then(async (u) => {
        if (!u.checkRole("STUDENTE")) {
            return res.status(404).json({ statusCode: 404, error: true, errormessage: "Unauthorized" });
        }
        else {
            var newInserzione = req.body;
            var newBook = newInserzione.book;
            //var countBook = await book.getModel().count({}) + 1
            var countInserzioni = await inserzioni.getModel().find().then(function (documents) {
                if (documents.length == 0) {
                    return 1
                }
                else {
                    var app = 0
                    for (var i = 0; i < documents.length; i++) {
                        if (app <= documents[i].inserzioneId)
                            app = documents[i].inserzioneId
                    }
                    app = app + 1
                    return app++
                }
            }).catch(function (reason) {
                return res.status(400).json({ statusCode: 404, error: true });
            });

            //Inserisce l'inserzione
            newInserzione.inserzioneId = countInserzioni
            inserzioni.getModel().create(newInserzione).then(function (data) {
                return res.status(200).json({ error: false, errormessage: "", id: data });
            }).catch(function (reason) {
                return res.status(400).json({ statusCode: 404, error: true });
            });
        }
    })
})

//Aumenta il prezzo dell'inserzione
app.put('/inserzioni/:idInserzione/offerta/:prezzo', auth, (req, res, next) => {
    user.getModel().findOne({ username: req.user.username }).then(async (u) => {
        if (!u.checkRole("STUDENTE")) {
            return res.status(404).json({ statusCode: 404, error: true, errormessage: "Unauthorized" });
        }
        else {

            inserzioni.getModel().findOne({ inserzioneId: req.params.idInserzione }).then((u) => {
                if (!u)
                    return next({ statusCode: 404, error: true, errormessage: "Inserzione non esistente" });
                else {
                    if (req.params.prezzo > u.prezzoAttuale) {
                        u.prezzoAttuale = req.params.prezzo
                        u.userWinner = req.user.username
                    }
                    else {
                        return next({ statusCode: 404, error: true, errormessage: "Offerta non valida" });
                    }
                    user.getModel().findOne({ username: req.user.username }).then((user) => {
                        if (user.astePartecipate.indexOf(req.params.idInserzione) < 0) {
                            user.astePartecipate.push(req.params.idInserzione)
                        }
                        user.save()
                    })
                    u.save().then((data) => {
                        return res.status(200).json({ error: false, errormessage: "", message: "Offerta inserita" });
                    }).catch((reason) => {
                        return next({ statusCode: 404, error: true, errormessage: "DB error: " + reason.errmsg });
                    });
                }
            });
        }
    })
});

//Modifica l'inserzione
app.put('/inserzioni', auth, (req, res, next) => {
    user.getModel().findOne({ username: req.user.username }).then(async (u) => {
        if (!u.checkRole("MODERATORE")) {
            return next({ statusCode: 404, error: true, errormessage: "Unauthorized" });
        }
        else {
            if (!req.body) {
                return res.status(400).json({ statusCode: 404, error: true, message: 'Manca il body nella richiesta' });
            }
            else {
                inserzioni.getModel().findOne({ inserzioneId: req.body.inserzioneId }).then((u) => {
                    if (!u && !(req.body.inserzioneId == u.inserzioneId))
                        return next({ statusCode: 404, error: true, errormessage: "Inserzione non trovata" });
                    else {
                        var body = req.body;
                        if (body.data) {
                            u.data = body.data
                        }
                        if (body.ora) {
                            u.ora = body.ora
                        }
                        if (body.prezzoRiserva) {
                            u.prezzoRiserva = body.prezzoRiserva
                        }
                        if (body.prezzoPartenza) {
                            u.prezzoPartenza = body.prezzoPartenza
                        }
                        if (body.book) {
                            u.book = body.book
                        }
                        u.save().then((data) => {
                            return res.status(200).json({ error: false, errormessage: "", message: "Inserzione aggiornata" });
                        }).catch((reason) => {
                            return next({ statusCode: 404, error: true, errormessage: "DB error: " + reason.errmsg });
                        });
                    }
                });
            }
        }
    })
});

//Restituisce le inserzioni vinte da un utente
app.get('/inserzioni/winbyuser/:username', (req, res, next) => {
    user.getModel().findOne({ username: req.params.username }).then((us) => {
        if (!us)
            return next({ statusCode: 404, error: true, errormessage: "Utente non esistente" });
        else {
            var vinte = []
            inserzioni.getModel().find().then((inserzioni) => {
                for (var i = 0; i < us.asteVinte.length; i++) {
                    for (var j = 0; j < inserzioni.length; j++) {
                        if (inserzioni[j].inserzioneId === us.asteVinte[i]) {
                            vinte.push(inserzioni[j])
                        }
                    }
                }
                return res.status(200).json(vinte);
            }).catch((reason) => {
                return next({ statusCode: 404, error: true, errormessage: "DB error: " + reason });
            });
        }
    })
})

//Restituisce le inserzioni vinte da un utente e non notificate
app.get('/inserzioni/winbyusernotnotify/:username', (req, res, next) => {
    user.getModel().findOne({ username: req.params.username }).then((u) => {
        if (!u)
            return next({ statusCode: 404, error: true, errormessage: "Utente non esistente" });
        else {
            inserzioni.getModel().find({ userWinner: req.params.username, /*active:false */ }).then((inserzioni) => {
                var notNotify = []
                for (var i = 0; i < u.asteVinteDaNotificare.length; i++) {
                    for (var j = 0; j < inserzioni.length; j++) {
                        if (inserzioni[j].inserzioneId == u.asteVinteDaNotificare[i]) {
                            notNotify.push(inserzioni[j])
                        }
                    }
                }
                return res.status(200).json(notNotify);
            }).catch((reason) => {
                return next({ statusCode: 404, error: true, errormessage: "DB error: " + reason });
            });
        }
    })
})

//Elimina l'inserzione
app.delete("/inserzioni/:inserzioneId", auth, (req, res, next) => {
    user.getModel().findOne({ username: req.user.username }).then(async (u) => {
        if (!u.checkRole("STUDENTE")) {
            return res.status(404).json({ statusCode: 404, error: true, errormessage: "Unauthorized" });
        }
        else {
            if (req.params.inserzioneId) {
                inserzioni.getModel().deleteOne({ inserzioneId: req.params.inserzioneId }).then((data) => {
                    return res.status(200).json({ error: false, errormessage: "", message: "Inserzione cancellata" });
                }).catch((reason) => {
                    return next({ statusCode: 404, error: true, errormessage: "DB error: " + reason });
                });
            } else {
                return next({ statusCode: 404, error: true, errormessage: "Specificare l'id dell'inserzione" });
            }
        }
    });
})

//Restituisce le inserzioni vinte da un utente
app.get('/inserzioni/partecipate/:username', (req, res, next) => {
    user.getModel().findOne({ username: req.params.username }).then((us) => {
        if (!us)
            return next({ statusCode: 404, error: true, errormessage: "Utente non esistente" });
        else {
            var partecipate = []
            inserzioni.getModel().find().then((inserzioni) => {
                for (var i = 0; i < us.astePartecipate.length; i++) {
                    for (var j = 0; j < inserzioni.length; j++) {
                        if (inserzioni[j].inserzioneId === us.astePartecipate[i]) {
                            partecipate.push(inserzioni[j])
                        }
                    }
                }
                return res.status(200).json(partecipate);
            }).catch((reason) => {
                return next({ statusCode: 404, error: true, errormessage: "DB error: " + reason });
            });
        }
    })
})

//Restituisce le inserzioni vinte da un utente
app.get('/inserzione/message/:idIinserzione', (req, res, next) => {
    message.getModel().find({ inserzioneId: req.params.idIinserzione }).then((documents) => {
        return res.status(200).json(documents);
    }).catch((reason) => {
        return next({ statusCode: 404, error: true, errormessage: "DB error: " + reason });
    })
})

//Ritorna le inserzioni NON attive e senza vincitore
app.get('/inserzioni/notActive/notUserWinner', (req, res, next) => {
    inserzioni.getModel().find({ active: false, userWinner: "" }).then(function (documents) {
        return res.status(200).json(documents);
    }).catch((reason) => {
        return next({ statusCode: 404, error: true, errormessage: "DB error: " + reason });
    });
})

/**
 * 
 * MESSAGGI
 * 
 */
app.post("/message", auth, async function (req, res, next) {
    var newMessage = req.body;
    var countMessage = await message.getModel().count({}) + 1
    //Inserisce l'inserzione
    newMessage.messageId = countMessage
    user.getModel().findOne({ username: newMessage.mittente }).then((u) => {
        if (!u) {
            return next({ statusCode: 404, error: true, errormessage: "Mittente non esistente" })
        }
    })
    user.getModel().findOne({ username: newMessage.destinatario }).then((u) => {
        if (!u) {
            return next({ statusCode: 404, error: true, errormessage: "Destinatario non esistente" })
        }
    })
    inserzioni.getModel().findOne({ inserzioneId: newMessage.inserzioneId }).then((u) => {
        if (!u) {
            return next({ statusCode: 404, error: true, errormessage: "Inserzione non esistente" })
        }
    })
    message.getModel().create(newMessage).then(function (data) {
        ios.emit('broadcast', data);
        return res.status(200).json({ error: false, errormessage: "", message: data });
    }).catch(function (reason) {
        return res.status(400).json({ statusCode: 404, error: true });
    });
})





/**
 * BOOK
 *
 */
//Ritorna la lista dei libri
app.get("/book", function (req, res) {
    book.getModel().find().then(function (documents) {
        return res.status(200).json(documents);
    }).catch(function (reason) {
        return res.status(400).json({ statusCode: 404, error: true });
    });
});
//Inserisce il libro
app.post("/book", async function (req, res) {
    var newBook = req.body;
    var countBook = await book.getModel().count({}) + 1
    newBook.bookId = countBook
    book.getModel().create(newBook).then(function (data) {
        return res.status(200).json({ error: false, errormessage: "", id: data });
    }).catch(function (reason) {
        return res.status(400).json({ statusCode: 404, error: true });
    });
});
//Elimina il libro ,
app.delete("/book/:bookId", auth, (req, res, next) => {
    book.getModel().findOne({ bookId: req.params.bookId })
    if (req.params.bookId) {
        book.getModel().deleteOne({ bookId: req.params.bookId }).then((data) => {
            return res.status(200).json({ error: false, errormessage: "", message: "Libro cancellato" });
        }).catch((reason) => {
            return next({ statusCode: 404, error: true, errormessage: "DB error: " + reason });
        });
    } else {
        return next({ statusCode: 404, error: true, errormessage: "Specificare l'id del libro" });
    }
})
//Ritorna un singolo libro
app.get('/book/:bookId', auth, (req, res, next) => {
    book.getModel().findOne({ bookId: req.params.bookId }).then((u) => {
        return res.status(200).json({ book: u });
    }).catch((reason) => {
        return next({ statusCode: 404, error: true, errormessage: "DB error: " + reason });
    });
})
//Modifica il libro
app.put('/book', auth, (req, res, next) => {
    book.getModel().findOne({ bookId: req.body.bookId }).then((u) => {
        if (!u && !(req.body.bookId == u.bookId))
            return next({ statusCode: 404, error: true, errormessage: "Libro non esistente" });
        else {
            var body = req.body;
            if (body.title) {
                u.title = body.title
            }
            if (body.university) {
                u.university = body.university;
            }
            if (body.course) {
                u.course = body.course;
            }
            u.save().then((data) => {
                return res.status(200).json({ error: false, errormessage: "", message: "Libro aggiornato" });
            }).catch((reason) => {
                return next({ statusCode: 404, error: true, errormessage: "DB error: " + reason.errmsg });
            });
        }
    });
});
//Ritorna un singolo libro per titolo
app.get('/book/title/:bookTitle', auth, (req, res, next) => {
    book.getModel().find({ title: req.params.bookTitle }).then((u) => {
        return res.status(200).json({ book: u });
    }).catch((reason) => {
        return next({ statusCode: 404, error: true, errormessage: "DB error: " + reason });
    });
})
//Ritorna un singolo libro per titolo
app.get('/book/course/:course', auth, (req, res, next) => {
    book.getModel().find({ course: req.params.course }).then((u) => {
        return res.status(200).json({ book: u });
    }).catch((reason) => {
        return next({ statusCode: 404, error: true, errormessage: "DB error: " + reason });
    });
})
//RItorna i libri di un'università
app.get('/book/university/:university', auth, (req, res, next) => {
    book.getModel().find({ university: req.params.university }).then((u) => {
        return res.status(200).json({ book: u });
    }).catch((reason) => {
        return next({ statusCode: 404, error: true, errormessage: "DB error: " + reason });
    });
})



/*
USER
*/
//Ritorna lista USERS
app.get('/users', auth, (req, res, next) => {
    user.getModel().findOne({ username: req.user.username }).then(async (u) => {
        if (!u.checkRole("MODERATORE")) {
            return res.status(404).json({ statusCode: 404, error: true, errormessage: "Unauthorized" });
        }
        else {

            user.getModel().find().then(function (documents) {
                if (documents.length == 0) {
                    return res.status(400).send('Nessun utente trovato');
                }
                else {
                    return res.status(200).json(documents);
                }
            }).catch(function (reason) {
                return res.status(400).json({ statusCode: 404, error: true });
            });
        }
    })
});

//Crea un nuovo utente
app.post('/user/student', (req, res, next) => {
    if (!req.body) {
        return res.status(400).json({ statusCode: 404, error: true, message: 'Manca il body nella richiesta' });
    }
    else {
        user.getModel().findOne({ username: req.body.username }).then((u) => {
            if (u)
                return next({ statusCode: 404, error: true, errormessage: "User already exists" });
            else {
                var u = user.newUser(req.body);
                if (!req.body.password) {
                    return next({ statusCode: 404, error: true, errormessage: "Password field missing" });
                }
                u.setPassword(req.body.password);
                u.role = "STUDENTE"
                u.save().then((data) => {
                    return res.status(200).json({ error: false, errormessage: "", username: data.username });
                }).catch((reason) => {
                    return next({ statusCode: 404, error: true, errormessage: "DB error: " + reason.errmsg });
                });
            }
        });
    }
});

//Crea un nuovo moderatore
app.post('/user/moderatore', auth, (req, res, next) => {
    console.log(req.user)
    console.log(res)
    user.getModel().findOne({ username: req.user.username }).then(async (u) => {
        if (!u.checkRole("MODERATORE")) {
            return res.status(404).json({ statusCode: 404, error: true, errormessage: "Unauthorized" });
        }
        else {

            if (!req.body) {
                return res.status(400).json({ statusCode: 404, error: true, message: 'Manca il body nella richiesta' });
            }
            else {
                user.getModel().findOne({ username: req.body.username }).then((u) => {
                    if (u)
                        return next({ statusCode: 404, error: true, errormessage: "User already exists" });
                    else {
                        var u = user.newUser(req.body);
                        if (!req.body.password) {
                            return next({ statusCode: 404, error: true, errormessage: "Password field missing" });
                        }
                        u.setPassword(req.body.password);
                        u.role = "MODERATORE"
                        u.nome = "MODERATORE"
                        u.cognome = "MODERATORE"
                        u.email = "MODERATORE"
                        u.regione = "MODERATORE"
                        u.save().then((data) => {
                            return res.status(200).json({ error: false, errormessage: "", username: data.username });
                        }).catch((reason) => {
                            return next({ statusCode: 404, error: true, errormessage: "DB error: " + reason.errmsg });
                        });
                    }
                });
            }
        }
    })
});

//Modifica l'utente
app.put('/user', auth, (req, res, next) => {
    if (!req.body) {
        return res.status(400).json({ statusCode: 404, error: true, message: 'Manca il body nella richiesta' });
    }
    else {
        user.getModel().findOne({ username: req.body.username }).then((u) => {
            if (!u && !(req.user.username == u.username))
                return next({ statusCode: 404, error: true, errormessage: "Unhautorized" });
            else {
                var body = req.body;
                if (body.password) {
                    u.setPassword(body.password);
                }
                if (body.email) {
                    u.email = body.email;
                }
                if (body.regione) {
                    u.regione = body.regione;
                }
                if (body.nome) {
                    u.nome = body.nome;
                }
                if (body.cognome) {
                    u.cognome = body.cognome;
                }
                u.save().then((data) => {
                    return res.status(200).json({ error: false, errormessage: "", message: "Utente aggiornato" });
                }).catch((reason) => {
                    return next({ statusCode: 404, error: true, errormessage: "DB error: " + reason.errmsg });
                });
            }
        });
    }
});

//Svuota la notifiche
app.put('/user/:username/emptyNotify', (req, res, next) => {
    user.getModel().findOne({ username: req.params.username }).then((u) => {
        while (u.asteVinteDaNotificare.length > 0) {
            u.asteVinteDaNotificare.pop()
        }
        while (u.asteFiniteDaNotificare.length > 0) {

            u.asteFiniteDaNotificare.pop()
        }
        u.save().then((data) => {
            return res.status(200).json({ error: false, errormessage: "", message: "Utente aggiornato" });
        }).catch((reason) => {
            return next({ statusCode: 404, error: true, errormessage: "DB error: " + reason.errmsg });
        });
    });
});

//Ritorna un User
app.get('/user/:username', auth, (req, res, next) => {
    if (!req.params.username) {
        return res.status(400).json({ statusCode: 404, error: true, message: 'Username mancante' });
    }
    else {
        user.getModel().findOne({ username: req.params.username }).then((u) => {
            return res.status(200).json(u);
        }).catch((reason) => {
            return next({ statusCode: 404, error: true, errormessage: "DB error: " + reason });
        });
    }
})

//Le inserzioni vinte e da notificare
app.get('/user/:username/asteVinteDaNotificare', auth, (req, res, next) => {
    if (!req.params.username) {
        return res.status(400).json({ statusCode: 404, error: true, message: 'Username mancante' });
    }
    else {
        user.getModel().findOne({ username: req.params.username }).then((u) => {
            return res.status(200).json(u.asteVinteDaNotificare);
        }).catch((reason) => {
            return next({ statusCode: 404, error: true, errormessage: "DB error: " + reason });
        });
    }
})

//Le inserzioni finite e da notificare
app.get('/user/:username/asteFiniteDaNotificare', auth, (req, res, next) => {
    if (!req.params.username) {
        return res.status(400).json({ statusCode: 404, error: true, message: 'Username mancante' });
    }
    else {
        user.getModel().findOne({ username: req.params.username }).then((u) => {
            return res.status(200).json(u.asteFiniteDaNotificare);
        }).catch((reason) => {
            return next({ statusCode: 404, error: true, errormessage: "DB error: " + reason });
        });
    }
})

//Cancella l'utente
app.delete('/user/:username', auth, (req, res, next) => {
    user.getModel().findOne({ username: req.user.username }).then(async (u) => {
        if (!u.checkRole("MODERATORE")) {
            return res.status(404).json({ statusCode: 404, error: true, errormessage: "Unauthorized" });
        }
        else {
            user.getModel().findOne({ username: req.params.username }).then((u) => {
                if (req.params.username) {
                    user.getModel().deleteOne({ username: req.params.username }).then((data) => {
                        return res.status(200).json({ error: false, errormessage: "", message: "Utente cancellato" });
                    }).catch((reason) => {
                        return next({ statusCode: 404, error: true, errormessage: "DB error: " + reason });
                    });
                } else {
                    return next({ statusCode: 404, error: true, errormessage: "Specificare username" });
                }
            });
        }
    });
})

//Rinnova l'autorizzazione.
app.get('/renew', auth, (req, res, next) => {
    var tokendata = req.user;
    delete tokendata.iat;
    delete tokendata.exp;
    console.log("Renewing token for user " + JSON.stringify(tokendata));
    var token_signed = jsonwebtoken.sign(tokendata, process.env.JWT_SECRET, { expiresIn: '1h' });
    console.log(req.params.l);
    return res.status(200).json({ error: false, errormessage: "", token: token_signed, location: req.query.l });
});

passport.use(new passportHTTP.BasicStrategy(
    function (username, password, done) {

        // Delegate function we provide to passport middleware
        // to verify user credentials 

        console.log("New login attempt from " + username);
        user.getModel().findOne({ username: username }, (err, user) => {
            console.log(err)
            if (err) {
                return done({ statusCode: 500, error: true, errormessage: err });
            }
            if (!user) {
                return done({ statusCode: 500, error: true, errormessage: "Invalid user" });
            }
            if (user.validatePassword(password)) {
                return done(null, user);
            }
            return done({ statusCode: 500, error: true, errormessage: "Invalid password" });
        })
    }
));


// Login endpoint uses passport middleware to check
// user credentials before generating a new JWT
app.get("/login", passport.authenticate('basic', { session: false }), (req, res, next) => {
    // If we reach this point, the user is successfully authenticated and
    // has been injected into req.user

    // We now generate a JWT with the useful user data
    // and return it as response
    var tokendata = {
        username: req.user.username,
        role: req.user.role,
    };
    console.log("Login granted. Generating token");
    var token_signed = jsonwebtoken.sign(tokendata, process.env.JWT_SECRET, { expiresIn: '1h' });

    // Note: You can manually check the JWT content at https://jwt.io

    return res.status(200).json({ error: false, errormessage: "", token: token_signed });

});




//app.listen(port, () => console.log(`HTTP app listening on port ${port}!`));
mongoose.connect(process.env.MONGODB_URI).then(
    function onconnected() {
        console.log("Connected to MongoDB");

        // To start a standard HTTP server we directly invoke the "listen"
        // method of express application
        let server = http.createServer(app);
        ios = require('socket.io')(server);
      //DECOMMENTARE PER POPOLARE
        /* var users = [
            {
                username: "federico.moderatore",
                nome: "Federico",
                cognome: "Mariano",
                password: "federico",
                role: "MODERATORE",
                regione: "Veneto",
                email: "a@a.it",
                asteFiniteDaNotificare: [],
                astePartecipate: [],
                asteVinte: [],
                asteVinteDaNotificare: []
            },
            {
                username: "Antonio",
                nome: "Antonio",
                cognome: "Prova",
                password: "antonio",
                role: "STUDENTE",
                regione: "Veneto",
                email: "antonio@a.it",
                asteFiniteDaNotificare: [],
                astePartecipate: [2],
                asteVinte: [],
                asteVinteDaNotificare: []
            },
            {
                astePartecipate : [ 
                    3,  
                    1, 
                ],
                asteVinte : [ 
                    3
                ],
                asteFiniteDaNotificare : [],
                username : "Federico",
                password: "federico",
                role : "STUDENTE",
                regione: "Veneto",
                email: "a@a.it",
                asteVinteDaNotificare : [],
                nome : "Federico",
                cognome : "Mariano"
            }
        ];
        for (const us of users) {
            var u = user.newUser(us);
            u.setPassword(us.password);
            u.save().then(() => {
                console.log('Utente creato');
            })
        }
        var inser = [
            {
                inserzioneId: 1,
                data: "19/01/2021",
                ora: "19:09",
                prezzoRiserva: 15,
                prezzoPartenza: 10,
                prezzoAttuale: 11,
                insertByUser: "Antonio",
                userWinner: "",
                active: false,
                bookId: 0,
                book: {
                    bookId: 0,
                    title: "Concetti di Java",
                    university: "Venezia",
                    course: "Programmazione a oggetti",
                    user: "Antonio",
                    inserzioneId: 0
                },
            },
            {
                inserzioneId : 2,
                data : "19/01/2021",
                ora : "19:10",
                prezzoRiserva : 30,
                prezzoPartenza : 10,
                prezzoAttuale : 13,
                insertByUser : "Federico",
                userWinner : "",
                active : false,
                bookId : 0,
                book : {
                    bookId : 0,
                    title : "Angular",
                    university : "Milano",
                    course : "Tecnologie WEB",
                    user : "Federico",
                    inserzioneId : 0
                },
            },
            {
                inserzioneId : 3,
                data : "19/01/2021",
                ora : "19:20",
                prezzoRiserva : 30,
                prezzoPartenza : 30,
                prezzoAttuale : 31,
                insertByUser : "Antonio",
                userWinner : "Federico",
                active : false,
                bookId : 0,
                book : {
                    bookId : 0,
                    title : "C#",
                    university : "Torino",
                    course : "Programmazione base",
                    user : "Antonio",
                    inserzioneId : 0
                },
            }
            
        ]
        for (const ins of inser) {
            var i =  inserzioni.newInserzione(ins);
            i.save().then(() => {
                console.log('Inserzione creata');
            })
        }

        var messages = [
            {
                timestamp : ("2021-01-23T14:52:02.927Z"),
                messageId : 1,
                mittente : "Federico",
                destinatario : "Antonio",
                content : "Ciao, antonio mi sapresti dire le condizioni del libro?\n",
                public : true,
                inserzioneId : 1,
            },
            {
                timestamp : ("2021-01-23T14:52:53.278Z"),
                messageId : 2,
                mittente : "Antonio",
                destinatario : "Federico",
                content : "Ciao Federico, il libro è in perfette condizioni, ha alcune sottolineature in alcune pagine.",
                public : true,
                inserzioneId : 1,
            }
        ]
        for (const mess of messages) {
            var m =  message.newMessage(mess);
            m.save().then(() => {
                console.log('Messaggio isnerito');
            })
        }*/

        server.listen(port, () => console.log("HTTP Server started on port" + port));
        ios.on('connection', socket => {
            console.log("Socket.io client connected");

        });

        var minutes = 1, the_interval = minutes * 10000;
        setInterval(async function () {
            var today = new Date();
            var minute = String(today.getMinutes());
            var hour = String(today.getHours());
            if (minute.length === 1) {
                minute = '0' + minute
            }
            if (hour.length === 1) {
                hour = '0' + hour
            }
            var time = hour + ":" + minute

            var dd = String(today.getDate());
            var mm = String(today.getMonth() + 1); //January is 0!
            var yyyy = today.getFullYear();
            if (dd.length === 1) {
                dd = '0' + dd
            }
            if (mm.length === 1) {
                mm = '0' + mm
            }
            var completeDate = dd + "/" + mm + "/" + yyyy
            var a = await inserzioni.getModel().find().then(function (documents) {
                if (documents.length == 0) {
                    return undefined
                }
                else {
                    for (var i = 0; i < documents.length; i++) {
                        if (documents[i].ora === time && documents[i].data === completeDate) {
                            if (documents[i].prezzoRiserva <= documents[i].prezzoAttuale) {
                                documents[i].active = false;
                                documents[i].save()
                                var idInserzione = documents[i].inserzioneId
                                user.getModel().findOne({ username: documents[i].userWinner }).then((u) => {
                                    u.asteVinte.indexOf(idInserzione) < 0 ? u.asteVinte.push(idInserzione) : null
                                    u.asteVinteDaNotificare.indexOf(idInserzione) < 0 ? u.asteVinteDaNotificare.push(idInserzione) : null
                                    u.save()
                                })
                                user.getModel().findOne({ username: documents[i].insertByUser }).then((u) => {
                                    u.asteFiniteDaNotificare.indexOf(idInserzione) < 0 ? u.asteFiniteDaNotificare.push(idInserzione) : null
                                    u.save()
                                })
                                //socket.broadcast.emit('message-broadcast', "INSERZIONE CONCLUSA");
                            }
                            else {
                                documents[i].active = false;
                                documents[i].userWinner = "";
                                documents[i].save()
                                var idInserzione = documents[i].inserzioneId
                                user.getModel().findOne({ username: documents[i].insertByUser }).then((u) => {
                                    u.asteFiniteDaNotificare.indexOf(idInserzione) < 0 ? u.asteFiniteDaNotificare.push(idInserzione) : null
                                    u.save()
                                })
                            }
                        }
                    }
                    return documents;
                }
            })
        }, the_interval);

        // To start an HTTPS server we create an https.Server instance 
        // passing the express application middleware. Then, we start listening
        // on port 8443
        //
        /*
        https.createServer({
          key: fs.readFileSync('keys/key.pem'),
          cert: fs.readFileSync('keys/cert.pem')
        }, app).listen(8443);
        */

    },
    function onrejected() {
        console.log("Unable to connect to MongoDB");
        process.exit(-2);
    }
)