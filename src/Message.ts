import mongoose = require('mongoose');

export interface Message extends mongoose.Document {
    messageId: Number,
    mittente: string,
    destinatario: string,
    content: string,
    public: boolean,
    inserzioneId: Number,
    timestamp: Date,
}

var messageSchema = new mongoose.Schema({
    messageId: {
        type: mongoose.SchemaTypes.Number,
        required: true,
    },
    mittente: {
        type: mongoose.SchemaTypes.String,
        required: true,
    },
    destinatario: {
        type: mongoose.SchemaTypes.String,
        required: true,
    },
    content: {
        type: mongoose.SchemaTypes.String,
        required: true,
    },
    public: {
        type: mongoose.SchemaTypes.Boolean,
        required: true,
    },
    inserzioneId: {
        type: mongoose.SchemaTypes.Number,
        required: true,
    },
    timestamp: {
        type: mongoose.SchemaTypes.Date,
        required: true
    }
})

export function getSchema() { return messageSchema; }

// Mongoose Model
var messageModel;  // This is not exposed outside the model
export function getModel() : mongoose.Model< Message > { // Return Model as singleton
    if( !messageModel ) {
        messageModel = mongoose.model('Message', getSchema() )
    }
    return messageModel;
}

export function newMessage( data ): Message {
    var _messagemodel = getModel();
    var inserzione = new _messagemodel( data );
    return inserzione;
}