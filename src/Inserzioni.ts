import mongoose = require('mongoose');
import { Book } from "./Book";
var autoIncrement = require("mongodb-autoincrement");

let BookModel = require("./Book")
let BookSchema = BookModel.schema

export interface Inserzioni extends mongoose.Document {
    inserzioneId: number,
    data: string,
    ora : string,
    prezzoRiserva: number,
    prezzoPartenza: number,
    prezzoAttuale: number,
    insertByUser: string,
    userWinner: string,
    active: boolean,
    bookId: number,
    book: Book
    
}

var inserzioniSchema = new mongoose.Schema({
    inserzioneId:{
        type: mongoose.SchemaTypes.Number,
        required: true,
    },
    data: {
        type: mongoose.SchemaTypes.String,
        required: true,
    },
    ora: {
        type: mongoose.SchemaTypes.String,
        required: true,
    },
    prezzoRiserva: {
        type: mongoose.SchemaTypes.Number,
        required: true,
    },
    prezzoPartenza: {
        type: mongoose.SchemaTypes.Number,
        required: true,
    },
    prezzoAttuale: {
        type: mongoose.SchemaTypes.Number,
        required: true,
    },
    insertByUser: {
        type: mongoose.SchemaTypes.String,
        required: true,
    },
    userWinner: {
        type: mongoose.SchemaTypes.String,
        required: false,
    },
    active: {
        type: mongoose.SchemaTypes.String,
        required: true,
    },
    bookId: {
        type: mongoose.SchemaTypes.Number,
        required: true,
    },
    book:{
        type: BookModel,
    }
})

export function getSchema() { return inserzioniSchema }


var inserzioniModel;
export function getModel(): mongoose.Model<Inserzioni> {
    if (!inserzioniModel) {
        inserzioniModel = mongoose.model('Inserzioni', getSchema())
    }
    return inserzioniModel
}

export function newInserzione( data ): Inserzioni {
    var _inserzionimodel = getModel();
    var inserzione = new _inserzionimodel( data );
    return inserzione;
}