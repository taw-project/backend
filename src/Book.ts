import mongoose = require('mongoose');

// A message has some text content, a list of tags and a timestamp
//
export interface Book extends mongoose.Document {
    bookId: number;
    title: string,
    university: string,
    course: string,
    user: string,
    inserzioneId: Number,
}

var bookSchema = new mongoose.Schema({
    //TODO eliminare bookId e inserzioneId
    bookId: {
        type: mongoose.SchemaTypes.Number,
        required: false,
    },
    title: {
        type: mongoose.SchemaTypes.String,
        required: true,
    },
    university: {
        type: mongoose.SchemaTypes.String,
        required: true,
    },
    course: {
        type: mongoose.SchemaTypes.String,
        required: true,
    },
    user:{
        type: mongoose.SchemaTypes.String,
        required : true
    },
    inserzioneId: {
        type: mongoose.SchemaTypes.Number,
        required: true,
    },
})

export function getSchema() { return bookSchema }

var bookModel;
export function getModel(): mongoose.Model<Book> {
    if (!bookModel) {
        bookModel = mongoose.model('Book', getSchema())
    }
    return bookModel
}

